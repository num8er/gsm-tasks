# TEST TASK 1:


Need to implement complex multipoint authentication for web application.

Two tables should be used: ëusersí and ëas_usersí.
Only users with ëuser_type_idí = 6 from ëusersí should be used. Users from this table gather role GSM Admin.
For users from ëas_usersí roles described in ëas_user_typesí table.

There are 3 entry points for different roles:
1.	/admin/login
2.	/sales/login
3.	/site/login/pid/îhashî (where hash is client specific hash from corresponding table, like - site/login/pid/RVM1G5621DGYHI)

Restrictions by roles:
1.	Only GSM Admins are allowed to login

2.	GSM Admins, Sales Users/Admins, Sales Users/Admins are allowed to login.

3.	All user roles are allowed, but Client Admins are allowed to login only using own client hash (as_clients->hash).
	Any Client Admin has relation to Client table by ëclientIdí field.

Find in authDB.sql SQL dump for task.
Feel free to use any encrypt password mechanism you prefer. You donít need to use current password values.

For implementation use PHP 5.x
No any framework restrictions.

---

SOLUTION: DONE

Please go to: `http://gsm.num8er.me/` and check all required auth mechanisms

1) for `admin` realm it picks user from users table that has user_type_id = 6 and active = 1

2) for `sales` realm it picks as_user from as_users table where as_user_type_id in 102, 103 and active = 1
if user not found it picks admin user that active = 1 (without user_type_id checking)

3) for `site` realm it accepts all kinds of users,
but if `hash` passed like `/site/login/pid/{hash}` then it will login using as_clients record
without password checking

Also You can connect to mysql database to play with user credentials:
```
host: num8er.me
user: gsm_tasks
pass: gsm_tasks
```

p.s. password for all user acounts is: 123456

---

# TEST TASK 2:

Use PHP 5.x

Task:
Develop a function which validates string looking like this "[{}]"
Every opening bracket should have corresponding closing bracket of same type
"[{}]" is a correct string, "[{]}" is malformed one.


Usage: <your host>/validateString.php?i={input string}

Example: <your host>/validateString.php?i={[{{}

---

SOLUTION: Check file public/validateString.php also go to: `http://gsm.num8er.me/validateString.php?i={{}}]}`

---