<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('123456');
        $records = [
            [
                'id' => 1, 'client_id' => 1, 'user_type_id' => 7,
                'email' => 'superuser@globalstest.com', 'password' => $password,
                'first_name' => 'Super', 'last_name' => 'User', 'phone' => '111-333-3333',
                'active' => 1, 'screen_lock_flag' => 1
            ],
            [
                'id' => 2, 'client_id' => 1, 'user_type_id' => 6,
                'email' => 'jim@testnet.com', 'password' => $password,
                'first_name' => 'Jim', 'last_name' => 'Admin', 'phone' => '111-640-2772',
                'active' => 1, 'screen_lock_flag' => 0
            ],
            [
                'id' => 4, 'client_id' => 1, 'user_type_id' => 6,
                'email' => 'jul@testnet.com', 'password' => $password,
                'first_name' => 'Jul', 'last_name' => 'Admin', 'phone' => '111-718-8148',
                'active' => 1, 'screen_lock_flag' => 0
            ]
        ];
        foreach($records AS $record) {
            User::create($record);
        }
    }
}