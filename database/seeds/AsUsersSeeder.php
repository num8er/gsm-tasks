<?php

use App\Models\AsUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class AsUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('123456');
        $records = [
            [
                'id' => 2, 'client_id' => 1, 'email' => 'vl.kol@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 1,
                'name' => 'Test user', 'accepted_tou' => 1, 'as_user_type_id' => 100
            ],
            [
                'id' => 4, 'client_id' => 2, 'email' => 'ar.av@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 0, 'account_admin' => 1,
                'name' => 'Client user', 'accepted_tou' => 0, 'as_user_type_id' => 100
            ],
            [
                'id' => 6, 'client_id' => 1, 'email' => 'ar.mil@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Temp user', 'accepted_tou' => 1, 'as_user_type_id' => 100
            ],
            [
                'id' => 10, 'client_id' => 1, 'email' => 'as@a.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 0, 'account_admin' => 0,
                'name' => 'Test', 'accepted_tou' => 1, 'as_user_type_id' => 100
            ],
            [
                'id' => 11, 'client_id' => 1, 'email' => 'a@a.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Ku user', 'accepted_tou' => 1, 'as_user_type_id' => 100
            ],
            [
                'id' => 12, 'client_id' => 1, 'email' => 'k@k.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 0, 'account_admin' => 0,
                'name' => 'ki', 'accepted_tou' => 0, 'as_user_type_id' => 100
            ],
            [
                'id' => 13, 'client_id' => 1, 'email' => 'l@l.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 0, 'account_admin' => 0,
                'name' => 'l', 'accepted_tou' => 0, 'as_user_type_id' => 100
            ],
            [
                'id' => 29, 'client_id' => 33, 'email' => 'genusertest3@mailinator.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 1,
                'name' => 'Genusertest3', 'accepted_tou' => 0, 'as_user_type_id' => 100
            ],
            [
                'id' => 90, 'client_id' => 11, 'email' => 'vl.koov@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Vlad K', 'accepted_tou' => 0, 'as_user_type_id' => 100
            ],
            [
                'id' => 111, 'client_id' => 0, 'email' => 'adi.kol@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Vlad K', 'accepted_tou' => 0, 'as_user_type_id' => 103
            ],
            [
                'id' => 116, 'client_id' => 0, 'email' => 'test@gsmsds.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Test 1', 'accepted_tou' => 0, 'as_user_type_id' => 101
            ],
            [
                'id' => 188, 'client_id' => 0, 'email' => 'sales1@mail.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Sales Man 1', 'accepted_tou' => 0, 'as_user_type_id' => 103
            ],
            [
                'id' => 192, 'client_id' => 0, 'email' => 'sales13@mail.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Sales Man 5', 'accepted_tou' => 0, 'as_user_type_id' => 104
            ],
            [
                'id' => 209, 'client_id' => 0, 'email' => 'test@testtest.com', 'password' => $password,
                'invitation_date' => '2016-07-06 20:09:51', 'active' => 1, 'account_admin' => 0,
                'name' => 'Sales Man 5', 'accepted_tou' => 0, 'as_user_type_id' => 102
            ],
        ];
        foreach($records AS $record) {
            AsUser::create($record);
        }
    }
}
