<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AsUserTypesSeeder::class);
        $this->call(AsClientsSeeder::class);
        $this->call(AsUsersSeeder::class);
        $this->call(UsersSeeder::class);
    }
}
