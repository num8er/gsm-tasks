<?php

use App\Models\AsClient;
use Illuminate\Database\Seeder;

class AsClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id' => 1, 'reseller_id' => 1, 'name' => 'Test Client Inc', 'hash' => 'RVM1G5621DGYHI',
                'created_at' => '2015-01-06 09:42:55', 'updated_at' => '2015-01-06 09:42:55',
                'contact_name' => 'Test user', 'contact_phone' => '+109871231', 'contact_email' => 'vl.kol@testtest.com',
                'account_number' => 'RB109142', 'advantage_program_status' => 'no', 'geography' => 'US',
                'branch_number' => 'LB14', 'branch_name' => 'LABORA', 'territory' => 'CANADA',
                'sales_rep_contact_name' => 'John Belly', 'sales_rep_contact_email' => 'vl.kol@testtest.com',
                'date_invitation_sent' => '2016-07-06 10:32:21',
                'account_status' => 1, 'account_status_date' => '2015-03-16 22:36:54',
                'client_source_type_id' => 1, 'demo' => 0, 'type_of_practice' => 1, 'parent_id' => NULL,
                'billing_cycle' => 'monthly', 'type_of_billing' => 'central'
            ],
            [
                'id' => 11, 'reseller_id' => 1, 'name' => 'Test Client 2', 'hash' => 'AVF1G5621DG34D',
                'created_at' => '2015-01-06 09:42:55', 'updated_at' => '2015-01-06 09:42:55',
                'contact_name' => 'Jim MacGregor', 'contact_phone' => '678-640-2772', 'contact_email' => 'jim@testtest.com',
                'account_number' => 'TY123456', 'advantage_program_status' => 'yes', 'geography' => 'US',
                'branch_number' => '', 'branch_name' => '', 'territory' => '',
                'sales_rep_contact_name' => '', 'sales_rep_contact_email' => '',
                'date_invitation_sent' => '2015-03-19 14:12:27',
                'account_status' => 0, 'account_status_date' => '2015-04-24 06:47:27',
                'client_source_type_id' => 1, 'demo' => 0, 'type_of_practice' => 1, 'parent_id' => NULL,
                'billing_cycle' => 'monthly', 'type_of_billing' => 'central'
            ],
            [
                'id' => 22, 'reseller_id' => 1, 'name' => 'Test Client 3', 'hash' => 'BDV111D4E33F',
                'created_at' => '2015-01-06 09:42:55', 'updated_at' => '2015-01-06 09:42:55',
                'contact_name' => '', 'contact_phone' => '', 'contact_email' => 'ar.may@testtest.com',
                'account_number' => 'ACCNUMBER01', 'advantage_program_status' => 'yes', 'geography' => 'US',
                'branch_number' => '', 'branch_name' => '', 'territory' => '',
                'sales_rep_contact_name' => '', 'sales_rep_contact_email' => '',
                'date_invitation_sent' => '2015-03-19 14:12:27',
                'account_status' => 1, 'account_status_date' => '2015-04-24 06:47:27',
                'client_source_type_id' => 1, 'demo' => 0, 'type_of_practice' => 1, 'parent_id' => NULL,
                'billing_cycle' => 'monthly', 'type_of_billing' => 'central'
            ],
            [
                'id' => 33, 'reseller_id' => 1, 'name' => 'Metro-St Louis Pk-EQ-ADPI', 'hash' => '240408957',
                'created_at' => '2015-01-06 09:42:55', 'updated_at' => '2015-01-06 09:42:55',
                'contact_name' => 'Genusertest3', 'contact_phone' => '5128999', 'contact_email' => 'genusertest3@or.com',
                'account_number' => '240408957', 'advantage_program_status' => 'no', 'geography' => 'US',
                'branch_number' => '240', 'branch_name' => 'MINNESOTA', 'territory' => '80',
                'sales_rep_contact_name' => 'Pamela R Dubay', 'sales_rep_contact_email' => 'Pamela.Dubay@testtest.comm',
                'date_invitation_sent' => '2015-03-19 14:12:27',
                'account_status' => 1, 'account_status_date' => '2015-04-24 06:47:27',
                'client_source_type_id' => 1, 'demo' => 0, 'type_of_practice' => 1, 'parent_id' => NULL,
                'billing_cycle' => 'monthly', 'type_of_billing' => 'central'
            ]
        ];

        foreach($records AS $record) {
            AsClient::create($record);
        }
    }
}
