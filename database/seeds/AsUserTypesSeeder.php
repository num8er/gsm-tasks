<?php

use App\Models\AsUserType;
use Illuminate\Database\Seeder;

class AsUserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            ['id' => 100, 'type' => 'Client Admin'],
            ['id' => 101, 'type' => 'Support'],
            ['id' => 102, 'type' => 'Sales Admin'],
            ['id' => 103, 'type' => 'Sales User'],
            ['id' => 104, 'type' => 'Support Admin']
        ];

        foreach($records AS $record) {
            AsUserType::create($record);
        }
    }
}
