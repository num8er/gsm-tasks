<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('as_clients')) {
            return false;
        }

        Schema::create('as_clients', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->comment = 'AutoSDS clients';

            $table->increments('id');
            $table->integer('reseller_id');
            $table->string('name', 255);
            $table->string('hash', 50);
            $table->string('contact_name', 255)->comment = 'Primary contact name';
            $table->string('contact_phone', 100);
            $table->string('contact_email', 255);
            $table->string('account_number', 50)->comment = 'Reseller account number';
            $table->enum('advantage_program_status', ['no', 'yes']);
            $table->enum('geography', ['US', 'CAN']);
            $table->string('branch_number', 50);
            $table->string('branch_name', 255);
            $table->string('territory', 255);
            $table->string('sales_rep_contact_name', 100);
            $table->string('sales_rep_contact_email', 100);
            $table->dateTime('date_invitation_sent')->nullable();
            $table->tinyInteger('account_status')->default(1);
            $table->dateTime('account_status_date');
            $table->tinyInteger('client_source_type_id')->default(1);
            $table->boolean('demo')->default(0)->comment = 'Account using for demo';
            $table->unsignedInteger('type_of_practice')->default(1);
            $table->integer('parent_id')->nullable();
            $table->enum('billing_cycle', ['monthly', 'annually'])->default('monthly');
            $table->enum('type_of_billing', ['central', 'location'])->default('location');
            $table->timestamps();

            $table->unique('hash');
            $table->unique('account_number');
            $table->index('reseller_id');
            $table->index('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('as_clients');
    }
}
