<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')) {
            return false;
        }

        Schema::create('users', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->comment = 'AutoSDS client users';

            $table->increments('id');
            $table->integer('client_id');
            $table->integer('user_type_id')->default(0);
            $table->string('email', 80)->default('');
            $table->string('password', 64)->default('');
            $table->string('first_name', 80)->default('');
            $table->string('last_name', 80)->default('');
            $table->string('phone', 80)->default('');
            $table->boolean('active')->default(0);
            $table->boolean('screen_lock_flag')->default(0);

            $table->index('client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
