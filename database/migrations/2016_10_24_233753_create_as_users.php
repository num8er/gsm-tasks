<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('as_users')) {
            return false;
        }

        Schema::create('as_users', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->comment = 'AutoSDS client users';

            $table->increments('id');
            $table->integer('client_id');
            $table->string('email', 100);
            $table->string('password', 64)->comment = 'password';
            $table->dateTime('invitation_date')->comment = 'Date when invitation mail sent';
            $table->boolean('active')->default(1)->comment = 'User finished registration process';
            $table->boolean('account_admin')->default(0);
            $table->string('name', 255)->comment = 'name of user';
            $table->boolean('accepted_tou')->default(0)->comment = 'Acceptance of Terms of Use';
            $table->unsignedInteger('as_user_type_id')->default(100);

            $table->unique(['client_id', 'email']);
            $table->index('as_user_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('as_users');
    }
}
