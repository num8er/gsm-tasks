@extends('layouts.auth')

@section('title') Login page @stop

@section('js_header')
@stop

@section('js_footer')
    <script>
        $(function() {
            var $form = $('form:first');
            $form.validate({
                errorClass: 'help-block text-right animated fadeInDown',
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    $(e).parents('.form-group .form-material').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                    $(e).closest('.help-block').remove()
                },
                success: function(e) {
                    $(e).closest('.form-group').removeClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'password': {
                        required: true,
                        minlength: 5
                    }
                }
            });

            $form.submit(function(e) {
                e.preventDefault();

                if(!$form.valid()) {
                    return;
                }

                $form.find('button[type="submit"]').hide();
                var url = $form.attr('action');
                var method = $form.attr('method');
                $.ajax({
                    url: url,
                    type: method,
                    data: $form.serializeArray(),
                    success: function(response) {
                        if(response.success) {
                            window.location.href = (response.redirectTo)? response.redirectTo : '{{ url()->route(request()->get('realm', 'home')) }}';
                            return;
                        }

                        if(response.message) {
                            $.notify({message: response.message},{type: 'warning'});
                        }
                    },
                    complete: function() {
                        $form.find('button[type="submit"]').show();
                    }
                });
            });
        });
    </script>
@stop



@section('content')
<!-- Login Content -->
<div class="bg-white pulldown">
    <div class="content content-boxed overflow-hidden">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                <div class="push-30-t push-50 animated fadeIn">
                    <!-- Login Title -->
                    <div class="text-center">
                        <i class="fa fa-2x fa-circle-o-notch text-primary"></i>
                        <p class="text-muted push-15-t">{{ env('APP_NAME') }} | REALM: {{ strtoupper(request()->get('realm')) }}</p>
                    </div>
                    <!-- END Login Title -->

                    <!-- Login Form -->
                    <form class="js-validation-login form-horizontal push-30-t" action="{{ route('auth.attempt', ['realm' => request()->get('realm')]) }}" method="post">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="text" id="email" name="email" placeholder="E-Mail">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material form-material-primary floating">
                                    <input class="form-control" type="password" id="password" name="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-group push-30-t">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <button class="btn btn-sm btn-block btn-primary" type="submit">Log in</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Login Form -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Login Content -->
@stop