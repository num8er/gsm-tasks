<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- END Page Content -->
</main>
<!-- END Main Container -->