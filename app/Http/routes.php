<?php

Route::get('/', ['as' => 'home', function () {
    return view('welcome');
}]);

/* AUTH */
Route::group(['prefix' => 'auth'], function() {
    Route::get('/', ['as' => 'auth', 'uses' => 'AuthController@index']);
    Route::get('/pid/{hash}', ['as' => 'auth.attempt.by.hash', 'uses' => 'AuthController@attempt']);
    Route::post('/', ['as' => 'auth.attempt', 'uses' => 'AuthController@attempt']);
    Route::delete('/', ['uses' => 'AuthController@destroy']);
    Route::any('destroy', ['as' => 'auth.destroy', 'uses' => 'AuthController@destroy']);
});

// stub route to redirect to authentication page
// by passing realm as query param
Route::get('/{realm}/login', function($realm) {
    if(\App\Library\Auth::realmExist($realm)) { // if requested realm exist
        // redirecting from  /admin/login to /auth?realm=admin
        return redirect()->route('auth', compact('realm'));
    }
    return redirect()->route('home');
});

// pid based auth
Route::get('/site/login/pid/{hash}', function($hash) {
    $realm = 'site';
    return redirect()->route('auth.attempt.by.hash', compact('hash', 'realm'));
});
/* EOF: AUTH */


/* ADMIN ROUTES */
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['App\Http\Middleware\HasAccessToAdmin']
], function() {
    Route::any('/', ['as' => 'admin', 'uses' => 'DashboardController@index']);

});
/* EOF: ADMIN ROUTES */


/* SALES ROUTES */
Route::group([
    'prefix' => 'sales',
    'namespace' => 'Sales',
    'middleware' => ['App\Http\Middleware\HasAccessToSales']
], function() {
    Route::any('/', ['as' => 'sales', 'uses' => 'DashboardController@index']);

});
/* EOF: SALES ROUTES */


/* EOF: SITE ROUTES */
Route::group([
    'prefix' => 'site',
    'namespace' => 'Site',
    'middleware' => ['App\Http\Middleware\HasAccessToSite']
], function() {
    Route::any('/', ['as' => 'site', 'uses' => 'DashboardController@index']);

});
/* EOF: SITE ROUTES */