<?php namespace App\Http\Controllers;

use App\Traits\ResponseJson;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ResponseJson;
}