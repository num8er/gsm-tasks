<?php namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Redirect;


class DashboardController extends Controller
{

    public function index()
    {
        return view('site.dashboard.index');
    }
}