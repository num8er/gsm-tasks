<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Auth;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        if(!Auth::realmExist($request->get('realm'))) {
            return redirect()->route('auth', ['realm' => Auth::DEFAULT_REALM]);
        }
        return view('auth.index');
    }

    public function attempt(Request $request)
    {
        $realm = $request->get('realm');
        $auth = Auth::attempt($realm, $request);
        if ($auth === false) {
            return $request->ajax()?
                $this->forbidden('
                Access denied! <br/>
                Make sure that:<br/>
                1) Username and/or Password is valid<br/>
                2) Account not suspended.<br/>
                3) Have access to '.ucfirst($realm).' interface')
                : redirect()->route('auth', compact('realm'));
        }

        $redirectTo = $request->get('redirectTo', route($realm));
        return ($request->ajax())?
            $this->ok(null, compact('redirectTo')) : redirect($redirectTo);
    }

    public function destroy(Request $request)
    {
        $realms = $request->get('realm', Auth::REALMS);
        if (is_array($realms) AND !empty($realms)) {
            $realms = explode(',', $realms);
        }
        Auth::destroy($request, $realms);

        return redirect('/');
    }
}