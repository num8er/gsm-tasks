<?php namespace App\Http\Middleware;

use App\Library\Auth;
use Closure;
use Illuminate\Http\Request;

class HasAccessToAdmin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $realm = 'admin';
        if (Auth::hasAccessTo($realm, $request)) {
            return $next($request);
        }
        return redirect()->route('auth', compact('realm'));
    }
}
