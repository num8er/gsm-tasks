<?php namespace App\Library;

use App\Models\AsClient;
use App\Models\User;
use App\Models\AsUser;
use Illuminate\Http\Request;

class Auth
{
    const REALMS = 'admin,sales,site';
    const DEFAULT_REALM = 'site';
    const NOBODY = 'nobody';

    public static function realmExist($realm) {
        return in_array($realm, explode(',', self::REALMS));
    }

    public static function attempt($realm, Request $request)
    {
        if(!self::realmExist($realm)) {
            return false;
        }

        $email = $request->input('email');
        $password = $request->input('password');

        $hash = $request->input('hash');
        $skipPassword = false;
        if($hash) {
            $skipPassword = true;
            $User = AsClient::whereHash($hash)->first();
        }
        else {
            $User = null;
            switch ($realm) {
                case 'admin' :
                    $User = User::whereEmail($email)
                                ->whereIn('user_type_id', [6])
                                ->whereActive(1)
                                ->first();
                    break;

                case 'sales' :
                    $User = AsUser::whereEmail($email)
                                    ->whereActive(1)
                                    ->whereIn('as_user_type_id', [102, 103])->first();
                    break;

                case 'site' :
                    $User = AsUser::whereEmail($email)
                                    ->whereActive(1)
                                    ->first();
                    break;
            }

            if (!$User) {
                $User = User::whereEmail($email)
                                ->whereActive(1)
                                ->first();
            }
        }

        if (!$User OR (!$skipPassword AND !$User->checkPassword($password))) {
            return false;
        }

        $auth = [
            'timestamp' => time(),
            'model' => get_class($User),
            'id' => $User->id,
            'access_to' => []
        ];
        $auth = $request->session()->get('auth', $auth);
        if (!in_array($realm, $auth['access_to'])) {
            $auth['access_to'][] = $realm;
        }

        $request->session()->put('auth', $auth);

        return $auth;
    }

    public static function destroy(Request $request, $realm = null)
    {
        if (is_null($realm)) {
            $request->session()->forget('auth');
            return true;
        }

        $auth = $request->session()->get('auth');
        if (isset($auth['access_to'])) {
            $realms = (is_array($realm) AND !empty($realm)) ? $realm : [$realm];
            foreach ($realms AS $realm) {
                $key = array_search($realm, $auth['access_to']);
                unset($auth['access_to'][$key]);
            }
            $auth['access_to'] = array_values($auth['access_to']);
            if(sizeof($auth['access_to']) > 0) {
                $request->session()->put('auth', $auth);
            }
            else {
                $request->session()->forget('auth');
            }
            return true;
        }
        return false;
    }

    public static function hasAccessTo($realm, Request $request)
    {
        $auth = $request->session()->get('auth', null);

        return (isset($auth['access_to']))?
                in_array($realm, $auth['access_to'])
                : false;
    }
}