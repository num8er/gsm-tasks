<?php namespace App\Traits;

trait ResponseJson
{

    private $cookies = [];

    public function setCookie($key, $value = null, $lifetime = 60)
    {
        $this->cookies[$key] = [$value, $lifetime];
    }

    public function deleteCookie($key)
    {
        $this->setCookie($key, null, -1);
    }

    private $headers = [];

    public function setHeaders($key, $value)
    {
        $this->headers[$key] = $value;
    }

    public function responseJSON($toResponse)
    {
        $response = response()->json($toResponse);

        /*
        if (!empty($this->cookies)) {
            foreach ($this->cookies AS $key => $value) {
                $response->withCookie(cookie($key, $value[0], $value[1]));
            }
        }

        if (!empty($this->headers)) {
            foreach ($this->headers AS $key => $value) {
                $response->header($key, $value);
            }
        }
        */

        return $response;
    }

    public function responseError($status, $code, array $options = array(), $success = false, $err = null)
    {
        $response = [];
        $response['status'] = $status;
        $response['code'] = (int)$code;
        $response['success'] = (bool)$success;

        if (!is_null($err)) {
            $response['err'] = $err;
        }

        if (!empty($options)) {
            foreach ($options AS $key => $value) {
                $response[$key] = $value;
            }
        }

        return $this->responseJSON($response);
    }

    public function badRequest($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'Request contains incorrect input data',
            'messages' => $messages
        ];
        return $this->responseError('badRequest', 400, $options);
    }

    public function forbidden($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'Access to URL resource denied',
            'messages' => $messages
        ];
        return $this->responseError('forbidden', 403, $options);
    }

    public function notFound($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'Record not found',
            'messages' => $messages
        ];
        return $this->responseError('notFound', 404, $options);
    }

    public function methodNotAllowed($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'Method Not Allowed',
            'messages' => $messages
        ];
        return $this->responseError('methodNotAllowed', 405, $options);
    }

    public function systemError($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'System Error',
            'messages' => $messages
        ];
        return $this->responseError('systemError', 500, $options);
    }

    public function notImplemented($message = null, $messages = null)
    {
        $options = [
            'message' => ($message) ? $message : 'Not Implemented',
            'messages' => $messages
        ];
        return $this->responseError('notImplemented', 501, $options);
    }

    public function internalServerError($message = 'Internal Server Error', $messages = null)
    {
        return $this->systemError($message, $messages);
    }

    public function ok($data = null, array $options = array())
    {
        $response = [];
        $response['status'] = 'ok';
        $response['code'] = 200;
        $response['success'] = true;

        if ($data) {
            $response['data'] = $data;
        }

        if (!empty($options)) {
            foreach ($options AS $key => $value) {
                $response[$key] = $value;
            }
        }

        return $this->responseJSON($response);
    }

    public function exists($message = null, $redirectTo = null)
    {
        $message = (trim($message) == '') ? 'Record exists' : $message;
        if (is_null($redirectTo)) $redirectTo = '#';
        return $this->ok(null, ['exists' => true, 'message' => $message, 'redirectTo' => $redirectTo]);
    }

    public function notExists($message = null)
    {
        $message = (trim($message) == '') ? 'Record does not exist' : $message;
        return $this->ok(null, ['exists' => false, 'message' => $message]);
    }

    public function warn($message)
    {
        return $this->ok(null, ['success' => false, 'warning' => true, 'message' => $message]);
    }

    public function tryOrSystemError($closure)
    {
        try {
            return $closure();
        } catch (Exception $ex) {
            $this->systemError($ex->getMessage());
        }
    }
}