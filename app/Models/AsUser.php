<?php namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class AsUser extends Model
{
    protected $table = 'as_users';
    protected $fillable = [
        'id',
        'client_id',
        'email',
        'password',
        'salt',
        'invitation_date',
        'active',
        'account_admin',
        'name',
        'accepted_tou',
        'as_user_type_id'
    ];
    public $timestamps = false;

    public function checkPassword($password) {
        return Hash::check($password, $this->password);
    }

    public function userType() {
        return $this->hasOne('App\Models\AsUserType', 'as_user_type_id', 'id');
    }

    public function client() {
        return $this->hasOne('App\Models\AsClient', 'client_id', 'id');
    }
}
