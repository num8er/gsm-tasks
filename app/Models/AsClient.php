<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsClient extends Model
{
    protected $table = 'as_clients';
    protected $fillable = [
        'id',
        'reseller_id',
        'name',
        'hash',
        'contact_name',
        'contact_phone',
        'contact_email',
        'account_number',
        'advantage_program_status',
        'geography',
        'branch_number',
        'branch_name',
        'territory',
        'sales_rep_contact_name',
        'sales_rep_contact_email',
        'date_invitation_sent',
        'account_status',
        'account_status_date',
        'client_source_type_id',
        'demo',
        'type_of_practice',
        'parent_id',
        'billing_cycle',
        'type_of_billing',
        'created_at',
        'updated_at'
    ];
    public $timestamps = true;

    public function parent() {
        return $this->belongsTo('App\Models\AsClient', 'parent_id', 'id');
    }
}
