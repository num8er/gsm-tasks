<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsUserType extends Model
{
    protected $table = 'as_user_types';
    protected $fillable = ['id', 'type'];
    public $timestamps = false;
}
