<?php namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'id',
        'client_id',
        'user_type_id',
        'email',
        'password',
        'first_name',
        'last_name',
        'phone',
        'active',
        'screen_lock_flag'
    ];
    public $timestamps = false;

    public function checkPassword($password) {
        return Hash::check($password, $this->password);
    }
}
