<?php
/*

Use PHP 5.x

Task:
Develop a function which validates string looking like this "[{}]"
Every opening bracket should have corresponding closing bracket of same type
"[{}]" is a correct string, "[{]}" is malformed one.


Usage: <your host>/validateString.php?i={input string}

Example: <your host>/validateString.php?i={[{{}

*/


function validateString($inputString) {
    $bracketPairs = ['(' => ')', '{' => '}', '[' => ']'];
    $bracketStack = [];

    $chars = str_split($inputString);
    $lastChar = null;
    foreach($chars AS $char) {
        if (isset($bracketPairs[$char])) {
            $lastChar = $char;
            $bracketStack[] = $char;
        }
        else {
            if ($lastChar AND $char == $bracketPairs[$lastChar]) {
                array_pop($bracketStack);
                $lastChar = end($bracketStack);
            }
            else {
                return false;
            }
        }
    }

    return sizeof($bracketStack) === 0;
}

$inputString = $_GET['i'];

echo "'".$inputString."' is ";
echo validateString($inputString)?"correct":"incorrect";
